module.exports = function(grunt) {

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    concat: {
      dist: {
        src: ['node_modules/jquery/dist/jquery.min.js', 'node_modules/bootstrap-sass/assets/javascripts/bootstrap/modal.js', 'node_modules/bootstrap-sass/assets/javascripts/bootstrap/tooltip.js', 'node_modules/bootstrap-sass/assets/javascripts/bootstrap/collapse.js', 'src/bootstrap3-typeahead.min.js', 'src/main.js'],
        dest: 'dist/js/main.js'
      }
    },
    uglify: {
      options: {
        banner: '/*! <%= pkg.name %> <%= grunt.template.today("dd-mm-yyyy") %> */\n',
        compress: {},
        beautify: true
      },
      dist: {
        files: {
          'dist/js/main.min.js': ['<%= concat.dist.dest %>'],
          //'//Volumes/autopub/iamahornet.com/global/js/main.min.js': ['<%= concat.dist.dest %>'],
         //'//Volumes/autopub/www.emporia.edu/global/js/<%= pkg.name %>.min.js': ['<%= concat.dist.dest %>']
        }
      }
    },
    sass: {                              // Task
      dist: {                            // Target
        options: {                       // Target options
          style: 'compressed'
        },
        files: {                         // Dictionary of files
          'dist/css/main.css': 'src/main.scss',
          //'//Volumes/autopub/iamahornet.com/global/css/main.css': 'src/main.scss',
          //'//Volumes/autopub/www.emporia.edu/global/css/main.css': 'src/main.scss'      // 'destination': 'source'
        }
      }
    },
    watch: {
      scripts: {
        files: 'src/*.*',
        tasks: ['concat', 'uglify', 'sass'],
        options: {
          livereload: true
        },
      },
    }
  });

  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-serve');
  grunt.registerTask('default', ['concat', 'uglify', 'sass', 'watch', 'serve']);

};